# mage2-gulp-less by slashplus 
Gulp for Magento 2

This simple npm module allows you to setup [Gulp](https://gulpjs.com/) for Magento 2 easily in order to compile you 
themes less files. 

## Installation
1. Install the module via `npm install git+https://git@gitlab.com/slashplus-build/mage2-gulp-less#{tag|branch}` where 
`{tag|branch}` is one of the available releases (see tags) or one of the existing branches of the repository.
2. Make sure the gulpfile.js has been added to the Magento2 root automatically. 
If it is somehow not please [add the gulpfile.js manually](#reset-the-gulpfilejs-manually)
3. Create a `themes.loc.js` as described in the [Magento 2 DevDocs](https://devdocs.magento.com/guides/v2.3/frontend-dev-guide/tools/using_grunt.html#how-to-declare-custom-configuration-file-option-2) and optionally append a new
 custom theme object to the list of themes
5. Setup gulp in your environment configuration

## Setup the gulpfile.js
It is not necessary to install or setup a `gulpfile.js` at all. Whenever you run `npm install` or `npm update`, 
mage2-gulp-less will automatically update the gulpfile.js by copying it from the modules directory to the root. 
This way the `gulpfile.js` stays always up to date and you will never have to change the gulpfile itself.

### Reset the gulpfile.js manually
If you want to, you can also reset the gulpfile.js manually by copying it from `node_modules/mage2-gulp-less/` 
or simply by running the following command `npm explore mage2-gulp-less  -- npm run install-gulpfile`.

## Basic usage
#### CSS
`gulp css`

Compiles css for all themes (or a single theme if passed via [Parameter](#parameters)).

#### Watch
`gulp watch`

Compiles css and watches for changes for all themes (or a single theme if passed via [Parameter](#parameters)).
Uses `gulp css` internally.

#### Clean
`gulp clean`

Remove theme related static files in the pub/static and var directories,
by cleaning var/cache, var/view_preprocessed, pub/static/{YOUR_THEME} paths for all themes 
(or a single theme if passed via [Parameter](#parameters)).

### Parameters
Magento2 gulp-less allows the following optional parameters.

| Parameter | Value | Default |
| ------------- | ------------- | ------------- |
| `--map`      | Create Sourcemap | false |
| `--min`      | Minify output CSS | false |
| `--theme="yourThemeName"` <br><br>(multiple themes allowed: i.e. `--theme="themeA,themeB,themeC"`) | Theme name without namespace as defined in the Magento2 `themes.js`/`themes.loc.js`.<br>For example, `blank` or `backend`. | All Themes|

### Notices

#### Correctly symlink the files
Make sure you have run `php bin/magento dev:source-theme:deploy`
task with the corresponding flags before you run `css` or `watch`, in order to make sure the file symlinks 
have been set correctly.<br>
More information can be found in the [Magento 2 DevDocs](https://devdocs.magento.com/guides/v2.3/config-guide/cli/config-cli-subcommands-less-sass.html)

#### Passing the right theme objects
Dont forget to create a themes.loc.js and add add your custom theme.<br>
More information can be found in the [Magento 2 DevDocs](https://devdocs.magento.com/guides/v2.3/frontend-dev-guide/tools/using_grunt.html)

#### Use cases of tracking changes using Gulp (analogous to [Use cases of tracking changes using Grunt (Magento2 devdocs)](https://devdocs.magento.com/guides/v2.3/frontend-dev-guide/css-topics/css_debug.html#use_cases))
The following shows which Gulp tasks to use and in which order:

- After you switch the compilation mode from client-side to server-side, 
[symlink the files](#correctly-symlink-the-files).
- After you customize the content of any `.less` file, except the root source files, run `gulp css` task and reload  
the page.
- After you customize the root source files or move the files included to the root files, 
[symlink the files](#correctly-symlink-the-files) and reload the page.
- After you run `php bin/magento setup:upgrade`, [symlink the files](#correctly-symlink-the-files).
- After you [symlink the files](#correctly-symlink-the-files), run the `gulp clean` command to clear the Magento
 cache, then run the `gulp watch` command. 
 Running the commands in this order will ensure that any custom jQuery attributes like product sliders, banners, etc are loaded correctly.