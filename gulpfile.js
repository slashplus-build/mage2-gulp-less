'use strict';
let {src, dest, watch, series, parallel} = require('gulp');
let {argv} = require('yargs');
let colors = require('colors');
let sourcemaps = require('gulp-sourcemaps');
let less = require('gulp-less');
let cleanCSS = require('gulp-clean-css');
let gulpif = require('gulp-if');
let plumber = require('gulp-plumber');
let log = require('fancy-log');
let del = require('del');

// ==========================================================================
// Global parameters
// ==========================================================================
let themeName = argv.theme !== undefined ? argv.theme : '';
let isMap = argv.map !== undefined ? argv.map : false;
let isMinifyCss = argv.min !== undefined ? argv.min : false;

// ==========================================================================
// Global Magento 2 configs
// ==========================================================================
let filesRouter = require('./dev/tools/grunt/tools/files-router');
filesRouter.set('themes', './dev/tools/grunt/configs/themes');

let globalThemesConfig = require('./dev/tools/grunt/configs/themes.loc');
let globalLessConfig = require('./dev/tools/grunt/configs/less').options;

// ==========================================================================
// Helper functions
// ==========================================================================

function plumberErrorHandler(err) {
    if (nonzeroExitOnError !== true) return log(err);

    const exitCode = 2;
    log('[ERROR] gulp build task failed', err);
    log('[FAIL] gulp build task failed - exiting with code ' + exitCode);
    return process.exit(exitCode);
}

/**
 * Get the config object containing
 * the specific theme config object if defined as parameter (globally)
 * or all themes config objects
 *
 * @return Object
 */
function getConfig() {
    let themeConfig = {};
    if (themeName !== '') {
        themeConfig = getThemesConfig(themeName);
    } else {
        themeConfig = getAllThemesConfig();
    }
    return themeConfig;
}

/**
 * Get a specific theme config object if it has been defined in the global themes config
 *
 * @param themeKeys the name of the theme object keys (if more than one, divided by ',')
 * @throws Error
 * @return Object
 */
function getThemesConfig(themeKeys) {
    let configs = {};
    themeKeys = themeKeys.split(',');
    themeKeys.forEach(themeKey => {
        let config = getAllThemesConfig()[themeKey];

        if (config === undefined) {
            log.error(colors.red('Unknown theme name provided, can not find theme config for theme with key: ' + colors.bold(themeKey)));
            throw new Error();
        }

        configs[themeKey] = config;
    });

    return configs;
}

/**
 * Get the global themes config
 *
 * @throws Error
 * @return Object
 */
function getAllThemesConfig() {
    if (globalThemesConfig === undefined) {
        log.error(colors.red('Global theme config file invalid ' + themeName));
        throw new Error();
    }
    return globalThemesConfig;
}

/**
 * Get the theme config path (concatenation of area, name, locale), including a trailing slash.
 *
 * @param config the theme configuration object (defined in themes.loc.js)
 * @param base whether to return the path prefixed with the basepath ('./pub/static/') or not
 * @throws Error
 * @return Object
 */
function getThemePath(config, base = false) {
    let path = [config.area, config.name, config.locale].join('/') + '/';
    return base ? './pub/static/' + path : path;
}

function printDivider() {
    console.log(colors.green('======================================================================'));
}

// ==========================================================================
// Gulp tasks
// ==========================================================================
/**
 * Compile css for all themes or the passed theme(s) if passed using the global parameter
 */
function css(done) {
    let themeConfig = getConfig();

    if (Object.keys(themeConfig).length === 0) {
        return;
    }

    log('Running ' + colors.magenta('Less') + ' compilation for ' + colors.magenta(Object.keys(themeConfig).join(', ')));
    printDivider();

    Object.keys(themeConfig).forEach(themeKey => {
        let theme = themeConfig[themeKey];
        let themePath = getThemePath(theme, true);
        // concatenate the complete files paths
        let srcFiles = theme.files.map(file => themePath + file + '.' + theme.dsl);

        // Tell whats going on
        printDivider();
        log('Running ' + colors.magenta('Less') + ' compilation for ' + colors.magenta(themeKey + ' files (' + srcFiles.length + ')'));
        console.log(colors.green(srcFiles.join('\n')));

        // to the actual compiling
        src(srcFiles)
        // Source map
            .pipe(
                plumber({
                    errorHandler: err => {
                        log.error(colors.red(err));
                        this.emit('end');
                    }
                })
            )
            .pipe(gulpif(isMap, sourcemaps.init()))
            // Less compilation
            .pipe(less())
            // Minify CSS
            .pipe(gulpif(isMinifyCss, cleanCSS()))
            .pipe(gulpif(sourcemaps, sourcemaps.write('.')))
            .pipe(plumber.stop())

            // Destination folder
            .pipe(dest(themePath + 'css/'));
    });
    done();
}

/**
 * Watch changes of all themes or the passed theme(s) if passed using the global parameter.
 * Uses the `css` task.
 */
function watcher(done) {
    let themeConfig = getConfig();

    if (Object.keys(themeConfig).length === 0) {
        return;
    }

    Object.keys(themeConfig).forEach(themeKey => {
        let theme = themeConfig[themeKey];
        let themePath = getThemePath(theme, true);

        printDivider();
        log(colors.green('Watching: ' + theme.area + '/' + theme.name));
        printDivider();

        watch(themePath + '**/*.less', series('css'));
    });
    done();
}

/**
 * Remove theme related static files in the pub/static and var directories,
 * by cleaning var/cache, var/view_preprocessed, pub/static/{YOUR_THEME} paths.
 */
function clean() {
    let themeConfig = getConfig();

    if (Object.keys(themeConfig).length === 0) {
        return;
    }
    printDivider();
    log(colors.green('Cleaning: '));
    printDivider();

    log(colors.green('Cleaning "var/cache" & "var/view_preprocessed" ...'));
    let deleteList = [
        'var/cache',
        'var/view_preprocessed'
    ];

    Object.keys(themeConfig).forEach(themeKey => {
        let theme = themeConfig[themeKey];
        log(colors.green('Cleaning "pub/static/' + theme.area + '/' + theme.name + '" ...'));
        deleteList.push('pub/static/' + theme.area + '/' + theme.name + '/**/*.less');
        deleteList.push('pub/static/' + theme.area + '/' + theme.name + '/**/*.css');
    });
    return del(deleteList);
}
exports.css = css;
exports.watch = watcher;
exports.clean = clean;

process.on('SIGINT', function () {
    process.exit(0);
});
